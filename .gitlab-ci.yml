---
stages:
  - lint
  - test
  - build
  - release

lint:
  image: harbor.service.wobcom.de/dockerhub-proxy/golangci/golangci-lint:v1.41.1
  tags:
    - docker
    - ubuntu
  stage: lint
  script:
    - make lint

lint-ui:
  image: harbor.service.wobcom.de/dockerhub-proxy/library/node:14@sha256:b19c3c12733345aed5c0d7a2d38c6beed69609293f97ec55a18e4934fe582777
  tags:
    - docker
    - ubuntu
  stage: lint
  script:
    - make dep-ui
    - make lint-ui
  only:
    changes:
      - web-app/**/*
      - .gitlab-ci.yml
      - Makefile

test:
  image: harbor.service.wobcom.de/dockerhub-proxy/library/golang:1.16
  services:
    - docker:19.03.12-dind
  tags:
    - docker
    - dind
    - ubuntu
  stage: test
  except:
    variables:
      - $CI_COMMIT_TAG =~ /^helm-chart.*$/
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    DB_HOSTNAME: docker
  script:
    - make test

test-ui:
  image: harbor.service.wobcom.de/dockerhub-proxy/library/node:14@sha256:b19c3c12733345aed5c0d7a2d38c6beed69609293f97ec55a18e4934fe582777
  tags:
    - ubuntu
    - docker
  stage: test
  script:
    - CI=true make dep-ui
    - make test-ui
  only:
    changes:
      - web-app/**/*
      - .gitlab-ci.yml
      - Makefile

build:
  stage: build
  image:
    name: harbor.service.wobcom.de/dockerhub-proxy/goreleaser/goreleaser:v0.174.2
    entrypoint: ["/bin/sh", "-c"]
  tags:
    - ubuntu
    - docker
  dependencies:
    - lint
    - test
  script:
    - make build

build-ui:
  image: harbor.service.wobcom.de/dockerhub-proxy/library/node:14@sha256:b19c3c12733345aed5c0d7a2d38c6beed69609293f97ec55a18e4934fe582777
  tags:
    - ubuntu
    - docker
  stage: build
  script:
    - CI=true make dep-ui
    - make build-ui
  only:
    changes:
      - web-app/**/*
      - .gitlab-ci.yml

release:
  stage: release
  image: harbor.service.wobcom.de/dockerhub-proxy/library/docker:19.03.12-dind
  services:
    - docker:19.03.12-dind
  tags:
    - ubuntu
    - docker
    - dind
  only:
    refs:
      - tags
    variables:
      - $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(\-[aA-zZ|\d]+)?$/
  dependencies:
    - lint
    - test
  variables:
    DOCKER_REGISTRY: $CI_REGISTRY
    DOCKER_USERNAME: $CI_REGISTRY_USER
    DOCKER_PASSWORD: $CI_REGISTRY_PASSWORD
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    GIT_DEPTH: 0
  before_script:
    - docker info
  script:
    - docker pull harbor.service.wobcom.de/dockerhub-proxy/goreleaser/goreleaser:v0.174.2
    - >
      docker run --rm --privileged
      -v /var/run/docker.sock:/var/run/docker.sock
      -v $PWD:/src
      -w /src
      -e DOCKER_REGISTRY
      -e DOCKER_USERNAME
      -e DOCKER_PASSWORD
      -e GITLAB_TOKEN
      harbor.service.wobcom.de/dockerhub-proxy/goreleaser/goreleaser:v0.174.2
      release --rm-dist

release-ui:
  stage: release
  image: harbor.service.wobcom.de/dockerhub-proxy/library/docker:19.03.12-dind
  services:
    - docker:19.03.12-dind
  tags:
    - ubuntu
    - docker
    - dind
  only:
    refs:
      - tags
    variables:
      - $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(\-[aA-zZ|\d]+)?$/
  dependencies:
    - lint
    - test
  variables:
    DOCKER_REGISTRY: $CI_REGISTRY
    DOCKER_USERNAME: $CI_REGISTRY_USER
    DOCKER_PASSWORD: $CI_REGISTRY_PASSWORD
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    GIT_DEPTH: 0
  before_script:
    - docker info
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
  script:
   - cd web-app/
   - docker build -t harbor.service.wobcom.de/public/mqtt-auth-ui:$CI_COMMIT_TAG .
   - docker push harbor.service.wobcom.de/public/mqtt-auth-ui:$CI_COMMIT_TAG

lint-helm-chart:
  stage: lint
  tags:
    - ubuntu
    - docker
  image:
    name: harbor.service.wobcom.de/dockerhub-proxy/pixtadev/helm-push:v3.2.4
    entrypoint: ["/bin/sh", "-c"]
  only:
    changes:
      - deployments/helm/**/*.y*ml
      - deployments/helm/**/templates/*.*y*ml
      - deployments/helm/**/templates/*.tpl
      - .gitlab-ci.yml
      - Makefile
  script:
    - helm lint ./deployments/helm

push-helm-chart:
  stage: release
  tags:
    - ubuntu
    - docker
  image:
    name: harbor.service.wobcom.de/dockerhub-proxy/pixtadev/helm-push:v3.2.4
    entrypoint: ["/bin/sh", "-c"]
  only:
    refs:
      - tags
    variables:
      - $CI_COMMIT_TAG =~ /^helm-chart.*$/
  script:
    - export CHART="deployments/helm/"
    - export VERSION="${CI_COMMIT_TAG#*/}"
    - helm repo add public https://harbor.service.wobcom.de/chartrepo/public
    - echo "Push chart $CHART in version $VERSION"
    - helm push "./$CHART" --version="$VERSION" public