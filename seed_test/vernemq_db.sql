--
-- PostgreSQL database dump
--

-- Dumped from database version 12.7
-- Dumped by pg_dump version 12.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.vmq_auth_acl DROP CONSTRAINT vmq_auth_acl_unique;
ALTER TABLE ONLY public.vmq_auth_acl DROP CONSTRAINT vmq_auth_acl_pk;
ALTER TABLE public.vmq_auth_acl ALTER COLUMN acl_id DROP DEFAULT;
DROP SEQUENCE public.vmq_auth_acl_acl_id_seq;
DROP TABLE public.vmq_auth_acl;
SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: vmq_auth_acl; Type: TABLE; Schema: public; Owner: vernemq
--

CREATE TABLE public.vmq_auth_acl (
    acl_id integer NOT NULL,
    mountpoint character varying(10) NOT NULL,
    client_id character varying(128) NOT NULL,
    username character varying(128) NOT NULL,
    password character varying(128),
    publish_acl json,
    subscribe_acl json
);


ALTER TABLE public.vmq_auth_acl OWNER TO vernemq;

--
-- Name: vmq_auth_acl_acl_id_seq; Type: SEQUENCE; Schema: public; Owner: vernemq
--

CREATE SEQUENCE public.vmq_auth_acl_acl_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vmq_auth_acl_acl_id_seq OWNER TO vernemq;

--
-- Name: vmq_auth_acl_acl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vernemq
--

ALTER SEQUENCE public.vmq_auth_acl_acl_id_seq OWNED BY public.vmq_auth_acl.acl_id;


--
-- Name: vmq_auth_acl acl_id; Type: DEFAULT; Schema: public; Owner: vernemq
--

ALTER TABLE ONLY public.vmq_auth_acl ALTER COLUMN acl_id SET DEFAULT nextval('public.vmq_auth_acl_acl_id_seq'::regclass);


--
-- Data for Name: vmq_auth_acl; Type: TABLE DATA; Schema: public; Owner: vernemq
--

COPY public.vmq_auth_acl (acl_id, mountpoint, client_id, username, password, publish_acl, subscribe_acl) FROM stdin;
1		test-client	test-user	$2a$06$mjMWTbwPaRDR1qUYlNhVuuha3VKPikb8cwt9vo6TTp1J.htGWl9pC	[{"pattern": "a/b/c"}, {"pattern": "c/b/#"}]	[{"pattern": "a/b/c"}, {"pattern": "c/b/#"}]
2		test-client	test-user-no-publish	$2a$06$bjgRIcOuEXEJzl9V22LmgOb/6beSsz2BxhVAytWVNRT.DbUOEl3S2	\N	[{"pattern": "a/b/c"}, {"pattern": "c/b/#"}]
3		test-client	test-user-no-subscribe	$2a$06$NUObCTAkiRnfPImLJlpUNuo0GuSYsapAc3TtPBeJhnJKyI/l2eXSq	[{"pattern": "a/b/c"}, {"pattern": "c/b/#"}]	\N
4		test-client	test-user-no-publish-subscribe	$2a$06$kz2ktv2MH0hphz28jB.WAubTDOXclaZgPrHAYj3zvzvMpKitUboSq	\N	\N
\.


--
-- Name: vmq_auth_acl_acl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vernemq
--

SELECT pg_catalog.setval('public.vmq_auth_acl_acl_id_seq', 4, true);


--
-- Name: vmq_auth_acl vmq_auth_acl_pk; Type: CONSTRAINT; Schema: public; Owner: vernemq
--

ALTER TABLE ONLY public.vmq_auth_acl
    ADD CONSTRAINT vmq_auth_acl_pk PRIMARY KEY (acl_id);


--
-- Name: vmq_auth_acl vmq_auth_acl_unique; Type: CONSTRAINT; Schema: public; Owner: vernemq
--

ALTER TABLE ONLY public.vmq_auth_acl
    ADD CONSTRAINT vmq_auth_acl_unique UNIQUE (mountpoint, client_id, username);


--
-- PostgreSQL database dump complete
--

