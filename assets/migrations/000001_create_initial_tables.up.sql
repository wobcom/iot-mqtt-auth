CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
ALTER TABLE IF EXISTS vmq_auth_acl DROP CONSTRAINT IF EXISTS vmq_auth_acl_primary_key;
ALTER TABLE IF EXISTS vmq_auth_acl DROP CONSTRAINT IF EXISTS vmq_auth_acl_pk;
ALTER TABLE IF EXISTS vmq_auth_acl DROP CONSTRAINT IF EXISTS vmq_auth_acl_unique;
ALTER TABLE IF EXISTS vmq_auth_acl
ADD COLUMN IF NOT EXISTS acl_id serial;
ALTER TABLE IF EXISTS vmq_auth_acl
ADD CONSTRAINT vmq_auth_acl_pk PRIMARY KEY (acl_id);
ALTER TABLE IF EXISTS vmq_auth_acl
ADD CONSTRAINT vmq_auth_acl_unique UNIQUE (mountpoint, client_id, username);
CREATE TABLE IF NOT EXISTS vmq_auth_acl (
    acl_id serial,
    mountpoint character varying(10) NOT NULL,
    client_id character varying(128) NOT NULL,
    username character varying(128) NOT NULL,
    password character varying(128),
    publish_acl json,
    subscribe_acl json,
    CONSTRAINT vmq_auth_acl_pk PRIMARY KEY (acl_id),
    CONSTRAINT vmq_auth_acl_unique UNIQUE (mountpoint, client_id, username)
);