// nolint:gochecknoglobals
package assets

import "embed"

// Migrations stores the DB migration SQL files.
//go:embed migrations
var Migrations embed.FS

// ConfigTpl stores the template for the service configuration.
//go:embed config.tpl
var ConfigTpl string
