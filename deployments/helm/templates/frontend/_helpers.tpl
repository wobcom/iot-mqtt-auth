{{- define "mqtt-auth.frontend.labels" -}}
{{ include "mqtt-auth.labels" . }}
{{ include "mqtt-auth.frontend.selectorLabels" . }}
{{- end }}


{{/*
Selector labels
*/}}
{{- define "mqtt-auth.frontend.selectorLabels" -}}
app.kubernetes.io/name: {{ include "mqtt-auth.name" . }}-frontend
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}