//nolint:tagliatelle
package vernemq

import (
	"encoding/json"
	"fmt"

	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/storage"
)

const (
	publishQuery string = `
	SELECT publish_acl::TEXT 
	FROM vmq_auth_acl 
	WHERE mountpoint=$1 AND username=$2 
	limit 1`

	publishStrictQuery string = `
	SELECT publish_acl::TEXT 
	FROM vmq_auth_acl WHERE mountpoint=$1 AND client_id=$2 AND username=$3 
	limit 1`
)

// {
//     "username": "username",
//     "mountpoint": "",
//     "client_id": "client-id",
//     "qos": 1,
//     "topic": "some/topic",
//     "payload": "message payload",
//     "retain": false,
//     "properties": {
//     }
// }.
type PublishRequestM5 struct {
	PublishRequest
	Properties map[string]string `json:"properties"`
}

// PublishRequest example
// {
//     "username": "username",
//     "client_id": "clientid",
//     "mountpoint": "",
//     "qos": 1,
//     "topic": "a/b",
//     "payload": "hello",
//     "retain": false
// }.
type PublishRequest struct {
	TopicConf
	ClientID   string `json:"client_id"`
	Username   string `json:"username"`
	Mountpoint string `json:"mountpoint"`
	Retain     bool   `json:"retain"`
	Payload    string `json:"payload"`
}

func AuthorizePublish(pr PublishRequest) bool {
	var ACLRaw string

	var err error

	if config.C.General.StrictMode {
		err = storage.DB().Get(&ACLRaw, publishStrictQuery, pr.Mountpoint, pr.ClientID, pr.Username)
	} else {
		err = storage.DB().Get(&ACLRaw, publishQuery, pr.Mountpoint, pr.Username)
	}

	if err != nil {
		log.Error().Err(err).Msg("AuthorizePublish")

		return false
	}

	var ACLS []ACL

	jsonError := json.Unmarshal([]byte(ACLRaw), &ACLS)
	if jsonError != nil {
		log.Error().Err(jsonError).Msg("AuthorizePublish")

		return false
	}

	log.Debug().Str("action", "publish").Str("clientID", pr.ClientID).Str("username", pr.Username).
		Str("acls", fmt.Sprintf("%s", ACLS)).Send()

	for _, acl := range ACLS {
		if TopicsMatch(acl.Pattern, pr.Topic) {
			return true
		}
	}

	return false
}
