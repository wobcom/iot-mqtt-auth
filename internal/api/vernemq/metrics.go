// nolint: gochecknoglobals,exhaustivestruct
package vernemq

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var authOnRegisterHistogram = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name:    "auth_on_register_seconds",
	Help:    "Time take to process auth_on_register",
	Buckets: []float64{1, 2, 5, 6, 10},
}, []string{"code"})

var authOnSubscribeHistogram = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name:    "auth_on_subscribe_seconds",
	Help:    "Time take to process auth_on_register",
	Buckets: []float64{1, 2, 5, 6, 10},
}, []string{"code"})

var authOnPublishHistogram = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name:    "auth_on_publish_seconds",
	Help:    "Time take to process auth_on_register",
	Buckets: []float64{1, 2, 5, 6, 10},
}, []string{"code"})
