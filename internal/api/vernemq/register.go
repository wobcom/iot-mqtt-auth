//nolint:tagliatelle,exhaustivestruct
package vernemq

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/storage"
)

const (
	selectClientIDUsername string = `
	SELECT client_id as clientid, username 
	FROM vmq_auth_acl 
	WHERE mountpoint=$1 AND client_id=$2 AND username=$3 AND password=crypt($4, password) 
	limit 1`

	selectUsername string = `
	SELECT username 
	FROM vmq_auth_acl 
	WHERE mountpoint=$1 AND username=$2 AND password=crypt($3, password) 
	limit 1`
)

// RegisterRequestM5 example
// {
//     "peer_addr": "127.0.0.1",
//     "peer_port": 8888,
//     "mountpoint": "",
//     "client_id": "client-id",
//     "username": "username",
//     "password": "password",
//     "clean_start": true,
//     "properties": {}
// }.
type RegisterRequestM5 struct {
	RegisterRequest
	CleanStart bool              `json:"clean_start"`
	Properties map[string]string `json:"properties"`
}

// RegisterRequestM31 example
// {
//
//     "peer_port": 8888,
//     "username": "username",
//     "password": "password",
//     "mountpoint": "",
//     "client_id": "clientid",
//     "clean_session": false
// }.
type RegisterRequestM31 struct {
	RegisterRequest
	CleanSession bool `json:"clean_session"`
}

type RegisterRequest struct {
	PeerPort   int32  `json:"peer_port"`
	ClientID   string `json:"client_id"`
	Username   string `json:"username"`
	Mountpoint string `json:"mountpoint"`
	PeerAddr   string `json:"peer_addr"`
	Password   string `json:"password"`
}

type authResult struct {
	ClientID string
	Username string
}

// Authenticate a Register request
// strict checks against the clientID.
func AuthUsername(rr RegisterRequest) bool {
	ar := authResult{}

	// log.Warn().Msg("strict mode active.")
	err := storage.DB().Get(&ar, selectUsername, rr.Mountpoint, rr.Username, rr.Password)
	if err != nil {
		log.Error().Err(err).Msg("authenticate error")

		return false
	}

	if ar.Username != "" && rr.Username != "" {
		return ar.Username == rr.Username
	}

	log.Warn().Msgf("rr.Username: %s, ar.Username: %s", rr.Username, ar.Username)

	return false
}

func AuthUsernameAndClientID(rr RegisterRequest) bool {
	ar := authResult{}

	err := storage.DB().Get(&ar,
		selectClientIDUsername, rr.Mountpoint, rr.ClientID, rr.Username, rr.Password,
	)
	if err != nil {
		log.Error().Err(err).Msg("authenticate error")

		return false
	}

	if ar.ClientID != "" && ar.Username != "" {
		log.Debug().Str(
			"res.ClientID", ar.ClientID,
		).Str(
			"req.ClientID", rr.ClientID,
		).Str(
			"res.Username", ar.Username,
		).Str(
			"req.Username", rr.Username,
		).Send()

		return ar.ClientID == rr.ClientID && ar.Username == rr.Username
	}

	log.Warn().Msgf(
		"rr.ClientID: %s , rr.Username: %s, ar.ClientID: %s , ar.Username: %s",
		rr.ClientID, rr.Username, ar.ClientID, ar.Username,
	)

	return false
}
