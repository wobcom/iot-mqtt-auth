// nolint: funlen
package vernemq

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
)

type ProtocolVersion int

const (
	MQTTV31 = iota
	MQTTV5
)

const (
	resultAllowed    string = `{"result":"ok"}`
	resultNotAllowed string = `{"result": { "error": "not_allowed" }}`
)

// AuthOnRegister is.
func AuthOnRegister(c config.Config, version ProtocolVersion) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		start := time.Now()

		w.Header().Add("Content-Type", "application/json; charset=utf-8")

		var rr RegisterRequest

		code := http.StatusInternalServerError
		authenticated := false

		defer func() {
			httpDuration := time.Since(start)
			authOnRegisterHistogram.WithLabelValues(fmt.Sprintf("%d", code)).Observe(httpDuration.Seconds())
			log.Info().Str("action", "register").Str("clientID", rr.ClientID).Str("username", rr.Username).
				Bool("allowed", authenticated).
				Send()
		}()

		decoder := json.NewDecoder(r.Body)

		switch version {
		case MQTTV31:
			var m31Request RegisterRequestM31

			err := decoder.Decode(&m31Request)
			if err != nil {
				log.Error().Err(err).Send()

				code = http.StatusBadRequest
				w.WriteHeader(code)

				return
			}

			rr = m31Request.RegisterRequest
		case MQTTV5:
			var m5Request RegisterRequestM5

			err := decoder.Decode(&m5Request)
			if err != nil {
				log.Error().Err(err).Send()

				code = http.StatusBadRequest
				w.WriteHeader(code)

				return
			}

			rr = m5Request.RegisterRequest
		}

		if c.General.StrictMode {
			authenticated = AuthUsernameAndClientID(rr)
		} else {
			authenticated = AuthUsername(rr)
		}

		if authenticated {
			code = http.StatusOK

			w.Header().Add("cache-control", "max-age=300")

			_, err := w.Write([]byte(resultAllowed))
			if err != nil {
				log.Error().Err(err).Msg("authentication error")
			}

			return
		}

		_, errW := w.Write([]byte(resultNotAllowed))
		if errW != nil {
			log.Error().Err(errW).Msg("authentication error")
		}
	}
}

// AuthOnSubscribe is.
func AuthOnSubscribe(c config.Config, version ProtocolVersion) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json; charset=utf-8")

		var sr SubscribeRequest

		authorized := false

		start := time.Now()

		defer r.Body.Close()

		code := http.StatusInternalServerError

		defer func() {
			httpDuration := time.Since(start)
			authOnSubscribeHistogram.WithLabelValues(fmt.Sprintf("%d", code)).Observe(httpDuration.Seconds())

			topics := make([]string, len(sr.Topics))
			for idx, t := range sr.Topics {
				topics[idx] = t.Topic
			}

			log.Info().Str("action", "subscribe").Str("clientID", sr.ClientID).Str("username", sr.Username).
				Bool("allowed", authorized).Strs("topics", topics).
				Send()
		}()

		decoder := json.NewDecoder(r.Body)

		switch version {
		case MQTTV31:
			err := decoder.Decode(&sr)
			if err != nil {
				log.Error().Err(err)

				code = http.StatusBadRequest
				w.WriteHeader(code)

				return
			}
		case MQTTV5:
			var srM5 SubscribeRequestM5

			err := decoder.Decode(&srM5)
			if err != nil {
				log.Error().Err(err)

				code = http.StatusBadRequest
				w.WriteHeader(code)

				return
			}

			sr = srM5.SubscribeRequest
		}

		authorized, approvedTopics := AuthorizeSubscribe(sr)
		if authorized {
			w.Header().Add("cache-control", "max-age=300")

			subResponse := SubscribeResponse{Result: "ok", Topics: approvedTopics}

			res, jsonError := json.Marshal(subResponse)
			if jsonError != nil {
				log.Error().Err(jsonError)
			}

			_, err := w.Write(res)
			if err != nil {
				log.Error().Err(err)
			}

			return
		}

		_, errW := w.Write([]byte(resultNotAllowed))
		if errW != nil {
			log.Error().Err(errW)
		}
	}
}

// AuthOnPublish is.
func AuthOnPublish(c config.Config, version ProtocolVersion) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json; charset=utf-8")

		authorized := false
		start := time.Now()
		code := http.StatusInternalServerError

		var pr PublishRequest

		defer r.Body.Close()

		defer func() {
			httpDuration := time.Since(start)
			authOnPublishHistogram.WithLabelValues(fmt.Sprintf("%d", code)).Observe(httpDuration.Seconds())

			log.Info().Str("action", "publish").Str("clientID", pr.ClientID).Str("username", pr.Username).
				Bool("allowed", authorized).Str("topic", pr.Topic).
				Send()
		}()

		decoder := json.NewDecoder(r.Body)

		switch version {
		case MQTTV31:
			err := decoder.Decode(&pr)
			if err != nil {
				log.Error().Err(err)

				code = http.StatusBadRequest
				w.WriteHeader(code)

				return
			}
		case MQTTV5:
			var prM5 PublishRequestM5

			err := decoder.Decode(&prM5)
			if err != nil {
				log.Error().Err(err)

				code = http.StatusBadRequest
				w.WriteHeader(code)

				return
			}

			pr = prM5.PublishRequest
		}

		authorized = AuthorizePublish(pr)
		if authorized {
			w.Header().Add("cache-control", "max-age=300")

			_, err := w.Write([]byte(resultAllowed))
			if err != nil {
				log.Error().Err(err)
			}

			return
		}

		_, errW := w.Write([]byte(resultNotAllowed))
		if errW != nil {
			log.Error().Err(errW)
		}
	}
}

func Setup(c config.Config, r *mux.Router) error {
	r.HandleFunc("/vernemq/auth_on_register",
		AuthOnRegister(config.C, MQTTV31)).Methods("POST")
	r.HandleFunc("/vernemq/auth_on_subscribe",
		AuthOnSubscribe(config.C, MQTTV31)).Methods("POST")
	r.HandleFunc("/vernemq/auth_on_publish",
		AuthOnPublish(config.C, MQTTV31)).Methods("POST")

	if config.C.General.M5Enabled {
		r.HandleFunc("/vernemq/auth_on_register_m5",
			AuthOnRegister(config.C, MQTTV5)).Methods("POST")
		r.HandleFunc("/vernemq/auth_on_subscribe_m5",
			AuthOnSubscribe(config.C, MQTTV5)).Methods("POST")
		r.HandleFunc("/vernemq/auth_on_publish_m5",
			AuthOnPublish(config.C, MQTTV5)).Methods("POST")
	}

	return nil
}
