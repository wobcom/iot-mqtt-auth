package vernemq

import "strings"

// Topic represents an mqtt topic with QoS.
type TopicConf struct {
	Topic string `json:"topic"`
	QoS   int    `json:"qos"`
}

// ACL contains the topic pattern to allow.
type ACL struct {
	Pattern string `json:"pattern"`
}

// TopicsMatch true if matched.
func TopicsMatch(savedTopic, givenTopic string) bool {
	return givenTopic == savedTopic || match(strings.Split(savedTopic, "/"), strings.Split(givenTopic, "/"))
}

func match(route []string, topic []string) bool {
	if len(route) == 0 {
		return len(topic) == 0
	}

	if len(topic) == 0 {
		return route[0] == "#"
	}

	if route[0] == "#" {
		return true
	}

	if (route[0] == "+") || (route[0] == topic[0]) {
		return match(route[1:], topic[1:])
	}

	return false
}
