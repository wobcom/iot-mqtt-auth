//nolint:wrapcheck
package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/wobcom/iot/mqtt-auth/graph/generated"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/auth"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/gql/models"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/errs"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/storage"
)

func (r *mutationResolver) CreateUser(ctx context.Context, data models.UserInput) (*models.User, error) {
	if claims := auth.ForContext(ctx); claims == nil || !claims.IsAdmin() {
		return nil, errs.ErrNotAuthorized
	}

	return storage.CreateUser(data)
}

func (r *mutationResolver) UpdateUser(ctx context.Context, data models.UpdateUserInput) (*models.User, error) {
	if claims := auth.ForContext(ctx); claims == nil || !claims.IsAdmin() {
		return nil, errs.ErrNotAuthorized
	}

	return storage.UpdateUser(int64(data.ID), data)
}

func (r *mutationResolver) DeleteUser(ctx context.Context, id int) (*models.User, error) {
	if claims := auth.ForContext(ctx); claims == nil || !claims.IsAdmin() {
		return nil, errs.ErrNotAuthorized
	}

	return storage.DeleteUser(int64(id))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type mutationResolver struct{ *Resolver }
