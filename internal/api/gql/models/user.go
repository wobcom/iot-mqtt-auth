//nolint:tagliatelle
package models

type User struct {
	ID         int64  `db:"acl_id"`
	Username   string `db:"username"`
	ClientID   string `db:"client_id"`
	MountPoint string `db:"mountpoint"`
	Subscribe  ACL    `db:"subscribe_acl"`
	Publish    ACL    `db:"publish_acl"`
}

type UserInput struct {
	Username   string                    `json:"username"`
	ClientID   string                    `json:"clientID"`
	MountPoint string                    `json:"mountPoint"`
	Password   string                    `json:"password"`
	Subscribe  []AccessControlEntryInput `json:"subscribe"`
	Publish    []AccessControlEntryInput `json:"publish"`
}

type UpdateUserInput struct {
	ID         int                        `json:"id"`
	Username   *string                    `json:"username"`
	ClientID   *string                    `json:"clientID"`
	MountPoint *string                    `json:"mountPoint"`
	Password   *string                    `json:"password"`
	Subscribe  []*AccessControlEntryInput `json:"subscribe"`
	Publish    []*AccessControlEntryInput `json:"publish"`
}

type UserFilter struct {
	Username   *string `json:"username"`
	ClientID   *string `json:"clientID"`
	MountPoint *string `json:"mountPoint"`
}
