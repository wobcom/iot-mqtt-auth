// nolint: gochecknoglobals
package config

import "time"

// Config defines the configuration structure.
type Config struct {
	General struct {
		LogLevel          int           `mapstructure:"log_level"`
		ShutdownTimeout   time.Duration `mapstructure:"shutdown_timeout"`
		StrictMode        bool          `mapstructure:"strict_mode"`
		M5Enabled         bool          `mapstructure:"enable_mqtt_v5"`
		PlaygroundEnabled bool          `mapstructure:"enable_playground"`
	}

	Auth struct {
		Enabled          bool   `mapstructure:"enable"`
		JWKSUrl          string `mapstructure:"jwks_url"`
		IssuerURL        string `mapstructure:"issuer_url"`
		LogOutURL        string `mapstructure:"logout_url"`
		CallbackURL      string `mapstructure:"callback_url"`
		LogInSuccessURL  string `mapstructure:"login_success_url"`
		LogOutSuccessURL string `mapstructure:"logout_success_url"`
		ClientID         string `mapstructure:"client_id"`
		ClientSecret     string `mapstructure:"client_secret"`
		RolesConfig      `mapstructure:"roles"`
		AllowedOrigins   []string `mapstructure:"allowed_origins"`
	}

	PostgreSQL struct {
		DSN                string        `mapstructure:"dsn"`
		MaxOpenConnections int           `mapstructure:"max_open_connections"`
		MaxIdleConnections int           `mapstructure:"max_idle_connections"`
		ConnMaxLifetime    time.Duration `mapstructure:"max_conn_lifetime"`
		AutoMigrate        bool          `mapstructure:"auto_migrate"`
	} `mapstructure:"postgresql"`
}

type RolesConfig struct {
	Viewer string `mapstructure:"viewer"`
	Admin  string `mapstructure:"admin"`
}

// C holds the global configuration.
var C Config
