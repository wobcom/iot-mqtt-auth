package storage

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/gql/models"
)

var psql sq.StatementBuilderType = sq.StatementBuilder.PlaceholderFormat(sq.Dollar) //nolint:gochecknoglobals

const (
	getUserQuery string = `
	select acl_id, username, client_id, mountPoint, subscribe_acl, publish_acl
	from vmq_auth_acl
	where acl_id=$1
	LIMIT 1`

	getUsersQuery string = `
	select acl_id, username, client_id, mountPoint, subscribe_acl, publish_acl
	from vmq_auth_acl`

	createUserQuery string = `
	insert into vmq_auth_acl (mountpoint, client_id, username, password, publish_acl, subscribe_acl)
	values ($1, $2, $3, crypt($4, gen_salt('bf')), $5, $6)
	returning acl_id, username, client_id, mountPoint, subscribe_acl, publish_acl
	`

	deleteUserQuery string = `
	delete from vmq_auth_acl 
	where acl_id = $1 
	returning acl_id, username, client_id, mountPoint, subscribe_acl, publish_acl
	`
)

func Authenticate() bool {
	return false
}

func GetUser(userID int) (*models.User, error) {
	ar := models.User{} //nolint:exhaustivestruct

	err := DB().Get(&ar, getUserQuery, userID)
	if err != nil {
		log.Error().Err(err).Msg("GetUser")

		return nil, errors.Wrap(err, "GetUser Error")
	}

	return &ar, nil
}

func CreateUser(newUser models.UserInput) (*models.User, error) {
	ar := models.User{} //nolint:exhaustivestruct

	var subscribe models.ACLInput = newUser.Subscribe

	var publish models.ACLInput = newUser.Publish

	err := db.Get(
		&ar,
		createUserQuery,
		newUser.MountPoint,
		newUser.ClientID,
		newUser.Username,
		newUser.Password,
		publish,
		subscribe,
	)
	if err != nil {
		log.Error().Err(err).Msg("CreateUser")

		return nil, errors.Wrap(err, "CreateUser Error")
	}

	return &ar, nil
}

//nolint:cyclop
func UpdateUser(userID int64, data models.UpdateUserInput) (*models.User, error) {
	ar := models.User{} //nolint:exhaustivestruct

	q := psql.Update("vmq_auth_acl").Where(sq.Eq{"acl_id": userID})

	if data.ClientID != nil {
		q = q.Set("client_id", *data.ClientID)
	}

	if data.MountPoint != nil {
		q = q.Set("mountpoint", *data.MountPoint)
	}

	if data.Username != nil && *data.Username != "" {
		q = q.Set("username", data.Username)
	}

	if data.Password != nil && *data.Password != "" {
		q = q.Set("password", *data.Password)
	}

	if data.Publish != nil {
		var publish models.ACLInputPtr = data.Publish
		q = q.Set("publish_acl", publish)
	}

	if data.Subscribe != nil {
		var subscribe models.ACLInputPtr = data.Subscribe
		q = q.Set("subscribe_acl", subscribe)
	}

	q = q.Suffix("returning acl_id, username, client_id, mountPoint, subscribe_acl, publish_acl")

	sql, args, err := q.ToSql()
	if err != nil {
		log.Error().Err(err).Msg("UpdateUser")

		return nil, errors.Wrap(err, "Dynamic Query Error")
	}

	err = db.Get(&ar, sql, args...)
	if err != nil {
		log.Error().Err(err).Msg("UpdateUser")

		return nil, errors.Wrap(err, "UpdateUser Error")
	}

	return &ar, nil
}

func DeleteUser(userID int64) (*models.User, error) {
	ar := models.User{} //nolint:exhaustivestruct

	err := db.Get(&ar, deleteUserQuery, userID)
	if err != nil {
		log.Error().Err(err).Msg("DeleteUser")

		return nil, errors.Wrap(err, "DeleteUser Error")
	}

	return &ar, nil
}

func GetUsers(filter *models.UserFilter) ([]*models.User, error) {
	ar := []*models.User{}

	err := DB().Select(&ar, getUsersQuery)
	if err != nil {
		log.Error().Err(err).Msg("GetUsers")

		return nil, errors.Wrap(err, "GetUsers Error")
	}

	return ar, nil
}
