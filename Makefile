.PHONY: build serve
VERSION := $(shell git describe --always |sed -e "s/^v//")

build:
	 GORELEASER_CURRENT_TAG=v0.0.0 GORELEASER_PREVIOUS_TAG=v0.0.0-prerelease goreleaser build --rm-dist --snapshot

release:
	goreleaser release --rm-dist --snapshot --skip-publish

test:
	\
		GOFLAGS=-mod=vendor \
		go test -v -race -cover ./...

lint:
	golangci-lint run --enable-all --disable=godox

# shortcuts for development

dep-update:
	go get -u ./...
	go test ./...
	go mod tidy
	go mod vendor

# make start ARGS="--config=./auth-service-example.toml"
start:
	@echo "Starting IoT MQTT Auth-Service"
	@echo 'Use: make start ARGS="--config=./auth-service-example.toml" to pass a configuration file'
	@echo ''


	./dist/mqtt-auth_linux_amd64/mqtt-auth $(ARGS)

start-darwin:
	@echo "Starting IoT MQTT Auth-Service" 
	@echo 'Use: make start ARGS="--config=./auth-service-example.toml" to pass a configuration file'
	@echo ''

	./dist/mqtt-auth_darwin_arm64/mqtt-auth $(ARGS)
db-seed:
	psql -h localhost -U vernemq -W vernemq_db < ./seed_test/vernemq_db.sql

lint-ui:
	cd ./web-app && yarn run lint

dep-ui:
	cd ./web-app && yarn install

test-ui:
	cd ./web-app && yarn run test --ci --watchAll=false --passWithNoTests

build-ui:
	cd ./web-app && yarn run build


start-ui:
	cd ./web-app && yarn run start



